import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import swat.utils.javascriptutils as javascriptutils
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

WebUI.comment('This test verifies a species record can be created my entering values in only required fields.')

WebUI.navigateToUrl(GlobalVariable.G_Host + '/species')

WebUI.comment('Open the Species Modal')
WebUI.click(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/New Species Button'))

WebUI.comment('Enter data in required fields only and save')
WebUI.waitForElementPresent(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Add New Species Modal'), 2)

WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Species Name Field'), 'test')

WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Genus Field'), 'test')

WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Species Field'), 'test')

WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Common Name Field'), 'test')

WebUI.click(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Save Button'))

WebUI.comment('Verify new record. Click on the Genus column in the data grid')
WebUI.waitForElementPresent(findTestObject('Admin/MainPage/SpeciesTab/SpeciesGrid/Genus Search Icon'), 10)
WebUI.click(findTestObject('Admin/MainPage/SpeciesTab/SpeciesGrid/Genus Search Icon'))

WebUI.delay(1)

//Click 'Equals' for column search
WebUI.comment('Click the Equals option in the Genus dropdown')
CustomKeywords.'swat.utils.javascriptutils.jQ_click'("span.dx-menu-item-text:eq(4)")

WebUI.delay(1)

WebUI.comment('Enter the name of the Genus value in the Genus column search box')
WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesGrid/Genus Column Search Field'), 'test')

WebUI.comment('Give the grid time to load')
WebUI.waitForJQueryLoad(1, FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Verify the text is found in the grid')
TextFound = CustomKeywords.'swat.utils.javascriptutils.findtextincolumn'('//*[@id="gridSpecies"]/div/div[6]/div/div/div[1]/div/table/tbody/tr[1]/td[1]/a',
	'test')

WebUI.comment('Clean Up: Delete the record to maintain a clean db and and run this test without it failing due to a duplicate record')
WS.sendRequestAndVerify(findTestObject('Admin/MainPage/SpeciesTab/SpeciesGrid/Del Test Species Record'))

