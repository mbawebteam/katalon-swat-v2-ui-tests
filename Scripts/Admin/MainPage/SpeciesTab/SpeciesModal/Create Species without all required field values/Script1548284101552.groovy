import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import swat.utils.javascriptutils as javascriptutils
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

WebUI.comment('Verifies user cannot create a new species without values populating all required fields.')

WebUI.navigateToUrl(GlobalVariable.G_Host + '/species')

WebUI.comment('Open the Species Modal')
WebUI.click(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/New Species Button'))

WebUI.comment('Enter data in required fields only and save')
WebUI.waitForElementPresent(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Add New Species Modal'), 2)

WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Species Name Field'), 'test')

WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Genus Field'), 'test')

WebUI.setText(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Common Name Field'), 'test')

WebUI.click(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Save Button'))

WebUI.waitForElementPresent(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Required Field Validation Error'), 2)

WebUI.verifyElementPresent(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Required Field Validation Error'), 2)