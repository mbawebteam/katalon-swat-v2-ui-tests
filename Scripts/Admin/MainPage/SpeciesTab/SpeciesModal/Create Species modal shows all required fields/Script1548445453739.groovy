import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import swat.utils.javascriptutils as javascriptutils
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

WebUI.comment('Verifies all required fields are labeld as "Required"')

WebUI.navigateToUrl(GlobalVariable.G_Host + '/species')

WebUI.comment('Open the Species Modal')
WebUI.click(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/New Species Button'))

WebUI.comment("Verify each required field says 'Required'")
WebUI.waitForElementPresent(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Add New Species Modal'), 2)

WebUI.verifyElementAttributeValue(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Species Name Field'), 'placeholder', ' Required', 1)

WebUI.verifyElementAttributeValue(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Genus Field'), 'placeholder', ' Required', 1)

WebUI.verifyElementAttributeValue(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Species Field'), 'placeholder', ' Required', 1)

WebUI.verifyElementAttributeValue(findTestObject('Admin/MainPage/SpeciesTab/SpeciesModal/Common Name Field'), 'placeholder', ' Required', 1)