import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import swat.utils.javascriptutils as javascriptutils
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

/*//Reset column search
CustomKeywords.'swat.utils.javascriptutils.jQ_click'("span.dx-menu-item-text:eq(6)")

//Get first 17 characters of username
SearchString = DisplayName.take(17)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersGrid/NameColumnSearchInput'), SearchString)

WebUI.delay(3)

WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersGrid/ColumnSearchNameMagnifyingGlass'))

WebUI.delay(1)

//Click 'Starts with' for column search
CustomKeywords.'swat.utils.javascriptutils.jQ_click'("span.dx-menu-item-text:eq(2)")

WebUI.delay(1)

WebUI.verifyTextPresent(DisplayName, false)

RowsReturned = CustomKeywords.'swat.utils.javascriptutils.numberoftablerows'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody')

if (WebUI.verifyEqual(RowsReturned,4)) {
	assert true
}
else assert false*/
//Reset column search
CustomKeywords.'swat.utils.javascriptutils.jQ_click'("span.dx-menu-item-text:eq(6)")

//Get first 17 characters of username
SearchString = DisplayName.take(17)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersGrid/NameColumnSearchInput'), SearchString)

WebUI.delay(3)

WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersGrid/ColumnSearchNameMagnifyingGlass'))

WebUI.delay(1)

//Click 'Contains' for column search
CustomKeywords.'swat.utils.javascriptutils.jQ_click'("span.dx-menu-item-text:eq(2)")

WebUI.delay(1)

'Get the number of rows returned'
RowsReturned = CustomKeywords.'swat.utils.javascriptutils.numberoftablerows'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody')

'Search the grid to see if text is found'
TextFound = CustomKeywords.'swat.utils.javascriptutils.findtextincolumn'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody',
	SearchString)

'Pass if both the corrrect number of rows was returned and text was found'
if ((WebUI.verifyEqual(RowsReturned,4)) && TextFound) {
	assert true
}
else assert false


