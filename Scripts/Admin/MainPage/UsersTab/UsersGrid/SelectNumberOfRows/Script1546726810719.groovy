import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersGrid/10Rows'))

WebUI.delay(3)

RowsReturned = CustomKeywords.'swat.utils.javascriptutils.numberoftablerows'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody')

if (WebUI.verifyEqual(RowsReturned,10)) {
	assert true
}
else assert false

WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersGrid/20Rows'))

WebUI.delay(3)

RowsReturned = CustomKeywords.'swat.utils.javascriptutils.numberoftablerows'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody')

if (WebUI.verifyEqual(RowsReturned,20)) {
	assert true
}
else assert false

WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersGrid/40Rows'))

WebUI.delay(3)

RowsReturned = CustomKeywords.'swat.utils.javascriptutils.numberoftablerows'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody')

if (WebUI.verifyEqual(RowsReturned,40)) {
	assert true
}
else assert false