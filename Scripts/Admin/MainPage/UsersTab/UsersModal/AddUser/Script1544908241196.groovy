import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersGrid/button_New User'))

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_First Name_firstName'), FirstName)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Last Name_lastName'), LastName)

WebUI.selectOptionByValue(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/select_Suffix'), Suffix, true)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Username_userName'), Username)

WebUI.setEncryptedText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Password_userPassword'), Password)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Display Name_displayName'), DisplayName)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Job Title_jobTitle'), JobTitle)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Email Address_email'), EmailAddress)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Organization_organizatio'), Organization)

WebUI.selectOptionByValue(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/select_OtherAcademicGovernment'), OrganizationType, 
    true)

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Organization Country_org'), OrganizationCountry)

if (UserRole == 'admin') {
    WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Admin_role'))
} else {
    WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_User Role_role'))
}

if (NGOReviewer == "y") {
	WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/input_Add to NGO Distribution'))
}

WebUI.setText(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/textarea_Notes_note'), Notes)

WebUI.click(findTestObject('Admin/MainPage/UsersTab/UsersModal/AddUser/button_Save'))

WebUI.delay(5)

