import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Verify Worldwide  value is first entry in Country column'
WebUI.navigateToUrl(GlobalVariable.G_Host + '/locations')

WebUI.waitForPageLoad(3)

'Click the Country Sub-division tab'
WebUI.click(findTestObject('Object Repository/Admin/MainPage/LocationsTab/Country Sub-division Tab'))

'Verify first value in COUNTRY column is Worldwide'
firstValue = WebUI.getText(findTestObject('Object Repository/Admin/MainPage/LocationsTab/Worldwide Value'))
WebUI.verifyMatch(firstValue, 'Worldwide', true)