import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.navigateToUrl(GlobalVariable.G_Host + '/methods')

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/a_Methods'))

WebUI.delay(1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/div_Parent Gear Type'))

WebUI.delay(1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/span_Aquaculture_dx-sort dx-so'))

WebUI.waitForElementVisible(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/span'), 1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/span'))

WebUI.delay(3)

WebUI.setText(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/input_Aquaculture_dx-textedito'), 'ka')

WebUI.delay(1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/div_Kathy FAO Method'))

WebUI.delay(1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/a_Edit'))

WebUI.waitForJQueryLoad(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/button_Cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/span_Aquaculture_dx-icon dx-ic'))

//WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/span_1'))
WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/button_New Method'))

WebUI.waitForJQueryLoad(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.focus(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/button_Cancel'))

WebUI.delay(1)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Tree Grid/button_Cancel'))

