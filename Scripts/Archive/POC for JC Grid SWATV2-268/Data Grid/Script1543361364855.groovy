import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.G_Host + '/species')

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/a_Species'))

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/div_COMMON NAME'))

WebUI.setText(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/input_Reset_dx-col-1'), 'shiner')

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/Common Name Search Icon'))

// This next step fails. Other options: find by sendKeys? scrollToElement kinda worked...
//WebUI.selectOptionByLabel(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/Common Name Reset'), 'Reset', false)
WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/Common Name Reset'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/a_Edit'), 3)

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/a_Edit'))

//WebUI.switchToFrame(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/Edit Box'), 3, FailureHandling.CONTINUE_ON_FAILURE)

//WebUI.waitForElementPresent(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/button_Cancel'), 3)

WebUI.waitForJQueryLoad(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.focus(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/button_Cancel'))

WebUI.click(findTestObject('Archive/POC for JC Grid SWATV2-268/Data Grid/button_Cancel'))

