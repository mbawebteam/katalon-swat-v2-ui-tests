import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.comment("STEP: Given User is logged in as an admin and the Methods tab is open")

WebUI.navigateToUrl(GlobalVariable.G_Host + '/methods')

WebUI.comment("STEP: When I expand the tree grid")

WebUI.click(findTestObject('Archive/Methods Tab/Aquaculture Tab/Aquaculture Tab Object'))

WebUI.comment("STEP: And I hover over the edit or delete icons for a sub-method")

WebUI.click(findTestObject('Archive/Methods Tab/Aquaculture Tab/Bottom Cultured Tree Click'))

WebUI.comment("STEP: Then I will see the action the icon will trigger as well as the sub-method")

if(!WebUI.verifyElementPresent(findTestObject('Archive/Methods Tab/Aquaculture Tab/Delete Bottom Culture'), 3, FailureHandling.OPTIONAL)) {
	KeywordUtil.markFailedAndStop("CUSTOM ERROR MESSAGE: Unable to verify the sub-method label.")
	}

if(!WebUI.verifyElementPresent(findTestObject('Archive/Methods Tab/Aquaculture Tab/Edit Bottom Culture'), 3, FailureHandling.OPTIONAL)) {
	KeywordUtil.markFailedAndStop("CUSTOM ERROR MESSAGE: Unable to verify the sub-method label.")
	}