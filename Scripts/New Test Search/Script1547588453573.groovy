import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger

KeywordLogger log = new KeywordLogger()

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.G_Host)

WebUI.click(findTestObject('Admin/MainPage/LoginPage/div_SWAT_site-header'))

WebUI.setText(findTestObject('Admin/MainPage/LoginPage/input_Username_UserName'), GlobalVariable.G_LoginUserAdmin)

WebUI.setEncryptedText(findTestObject('Admin/MainPage/LoginPage/input_Password_Password'), GlobalVariable.G_LoginPasswordAdmin)

WebUI.click(findTestObject('Admin/MainPage/LoginPage/button_Login'))

WebUI.click(findTestObject('Admin/MainPage/a_Users'))

returnedvalue = CustomKeywords.'swat.utils.javascriptutils.findtextincolumn'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody', 
    'test')

log.logWarning('test:=' + returnedvalue)

returnedvalue = CustomKeywords.'swat.utils.javascriptutils.findtextincolumn'('//div[@id=\'gridUsers\']/div/div[6]/div/div/div/div/table/tbody', 
    'Lorena Schwatz')

log.logWarning('Lorena Schwatz:=' + returnedvalue)

