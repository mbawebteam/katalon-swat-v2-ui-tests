
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String


def static "swat.utils.javascriptutils.jQ_click"(
    	String selector	
     , 	String iFrameSelector	) {
    (new swat.utils.javascriptutils()).jQ_click(
        	selector
         , 	iFrameSelector)
}

def static "swat.utils.javascriptutils.numberoftablerows"(
    	String tablexpath	) {
    (new swat.utils.javascriptutils()).numberoftablerows(
        	tablexpath)
}

def static "swat.utils.javascriptutils.findtextincolumn"(
    	String tablexpath	
     , 	String texttofind	) {
    (new swat.utils.javascriptutils()).findtextincolumn(
        	tablexpath
         , 	texttofind)
}

def static "swat.utils.javascriptutils.jQ_click"(
    	String selector	) {
    (new swat.utils.javascriptutils()).jQ_click(
        	selector)
}
