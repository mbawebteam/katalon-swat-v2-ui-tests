package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object G_Host
     
    /**
     * <p></p>
     */
    public static Object G_LoginUserAdmin
     
    /**
     * <p></p>
     */
    public static Object G_LoginPasswordAdmin
     
    /**
     * <p></p>
     */
    public static Object G_Cookie
     
    /**
     * <p></p>
     */
    public static Object G_Username
     
    /**
     * <p></p>
     */
    public static Object G_password
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['G_Host' : 'qa.seafoodwatchassessments.org', 'G_LoginUserAdmin' : 'katalonadmin', 'G_LoginPasswordAdmin' : '16fadgUL1aOe2On/7qwuSw==', 'G_Cookie' : '__RequestVerificationToken=xRyuvoFo6fJpNLFGkVdUEAsZ_YTbiRjoj8gWFG7o0TVfpEbJawIWpz4Fer94yRXb04ad1u6_IkI3sW0bI9MwZ331l18c4oHN5_XQuUyI0fE1; ASP.NET_SessionId=ez0m552u4ebkjm1feupgspap'])
        allVariables.put('Admin', allVariables['default'] + ['G_Host' : 'https://qa.seafoodwatchassessments.org', 'G_LoginUserAdmin' : 'katalonadmin', 'G_LoginPasswordAdmin' : '16fadgUL1aOe2On/7qwuSw=='])
        allVariables.put('OLD SWAT V2 QA', allVariables['default'] + ['G_Host' : 'http://swatv2qa.seafoodwatch.org', 'G_Username' : 'kathy', 'G_password' : 'p2G3PVfPK0k='])
        allVariables.put('User', allVariables['default'] + ['G_Host' : 'https://qa.seafoodwatchassessments.org', 'G_LoginUserAdmin' : 'katalonuser', 'G_LoginPasswordAdmin' : 'gaisqHzQcYYIOGyPWc0UTg=='])
        
        String profileName = RunConfiguration.getExecutionProfile()
        def selectedVariables = allVariables[profileName]
		
		for(object in selectedVariables){
			String overridingGlobalVariable = RunConfiguration.getOverridingGlobalVariable(object.key)
			if(overridingGlobalVariable != null){
				selectedVariables.put(object.key, overridingGlobalVariable)
			}
		}

        G_Host = selectedVariables["G_Host"]
        G_LoginUserAdmin = selectedVariables["G_LoginUserAdmin"]
        G_LoginPasswordAdmin = selectedVariables["G_LoginPasswordAdmin"]
        G_Cookie = selectedVariables["G_Cookie"]
        G_Username = selectedVariables["G_Username"]
        G_password = selectedVariables["G_password"]
        
    }
}
