#Author: your.email@your.domain.com
#Keywords Summary :
#Feature:
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Methods Tab
As a user I want to know exactly which sub-method I am updating or deleting.
 
@valid
Scenario: The user will know which sub-method will be updted or deleted when hovering over the edit or delete icons.
    Given User is logged in as an admin and the Methods tab is open
    When I expand the tree grid
    And I hover over the edit or delete icons for a sub-method
    Then I will see the action the icon will trigger as well as the sub-method