package methodsTab
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class Methods {
	/**
	 * As a user I want to know exactly which sub-method I am updating or deleting.
	 */
	@Given("User is logged in as an admin and the Methods tab is open")
	def User_is_logged_in_as_an_admin_and_the_Methods_tab_is_open() {

		WebUI.navigateToUrl(GlobalVariable.G_Host + '/methods')
	}

	@When("I expand the tree grid")
	def When_I_expand_the_tree_grid() {
		WebUI.click(findTestObject('Archive/Methods Tab/Aquaculture Tab/Aquaculture Tab Object'))
	}

	@And("I hover over the edit or delete icons for a sub-method")
	def And_I_hover_over_the_edit_or_delete_icons_for_a_sub_method() {
		WebUI.click(findTestObject('Archive/Methods Tab/Aquaculture Tab/Bottom Cultured Tree Click'))
	}

	@Then("I will see the action the icon will trigger as well as the sub-method")
	def Then_I_will_see_the_action_the_icon_will_trigger_as_well_as_the_sub_method() {

		WebUI.verifyElementPresent(findTestObject('Archive/Methods Tab/Aquaculture Tab/Delete Bottom Culture'), 3)
		WebUI.verifyElementNotPresent(findTestObject('Archive/Methods Tab/Aquaculture Tab/Edit Bottom Culture'), 3)
	}
}