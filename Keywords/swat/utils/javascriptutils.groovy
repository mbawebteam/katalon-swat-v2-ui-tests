package swat.utils
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import org.apache.commons.lang.StringUtils

class javascriptutils {


	@Keyword
	static void jQ_click(String selector, String iFrameSelector = null) {
		String js = "\$(arguments[0]).click();"
		if(iFrameSelector) {
			js = "\$(arguments[1]).contents().find(arguments[0]).click();"
		}
		WebUI.executeJavaScript(js, Arrays.asList(selector, iFrameSelector))
	}

	@Keyword
	static int numberoftablerows(String tablexpath) {
		WebDriver driver = DriverFactory.getWebDriver()
		'Locate table'
		WebElement Table = driver.findElement(By.xpath(tablexpath))
		'To locate rows of table it will Capture all the rows available in the table'
		List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
		'To calculate no of rows In table; first row is subtracted as this is the search row'
		int rows_count = rows_table.size() - 1
		return rows_count
	}

	@Keyword
	static boolean findtextincolumn(String tablexpath, String texttofind) {
		boolean valuetoreturn = false
		WebDriver driver = DriverFactory.getWebDriver()
		'Locate table'
		WebElement Table = driver.findElement(By.xpath(tablexpath))
		'To locate rows of table it will Capture all the rows available in the table'
		List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
		'To calculate no of rows In table; first row is subtracted as this is the search row'
		int rows_count = rows_table.size() - 1

		'Loop will execute for all the rows of the table'
		Loop:
		for (int row = 0; row < rows_count; row++) {
			'To locate columns(cells) of that specific row'
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

			'To calculate no of columns(cells) In that specific row'
			int columns_count = Columns_row.size()

			//println((('Number of cells In Row ' + row) + ' are ') + columns_count)

			'Loop will execute till the last cell of that specific row'
			for (int column = 0; column < columns_count; column++) {
				'It will retrieve text from each cell'
				String celltext = Columns_row.get(column).getText()

				'Checking if Cell text is matching with the expected value'
				//if (celltext == texttofind) {
				if (StringUtils.contains(celltext , texttofind)) {

					//'Getting the Country Name if cell text i.e Company name matches with Expected value'
					valuetoreturn = true
					'After getting the Expected value from Table we will Terminate the loop'
					break Loop;
				}
			}
			//			return valuetoreturn
		}
		return valuetoreturn
	}
}
