<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Sub-division Header</name>
   <tag></tag>
   <elementGuidId>b21b9287-0b7d-4735-afc8-896f16cc6876</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#dx-col-6 > div.dx-treelist-text-content.dx-text-content-alignment-left</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#dx-col-6 > div.dx-treelist-text-content.dx-text-content-alignment-left</value>
   </webElementProperties>
</WebElementEntity>
