<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Worldwide Value</name>
   <tag></tag>
   <elementGuidId>2e31dab3-abb4-487b-b86b-03ca7f47dc8f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#gridLocations > div > div.dx-treelist-rowsview.dx-treelist-nowrap.dx-scrollable.dx-visibility-change-handler.dx-scrollable-both.dx-scrollable-simulated.dx-scrollable-customizable-scrollbars > div > div > div.dx-scrollable-content > div > table > tbody > tr:nth-child(1) > td.dx-treelist-cell-expandable > div.dx-treelist-text-content</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#gridLocations > div > div.dx-treelist-rowsview.dx-treelist-nowrap.dx-scrollable.dx-visibility-change-handler.dx-scrollable-both.dx-scrollable-simulated.dx-scrollable-customizable-scrollbars > div > div > div.dx-scrollable-content > div > table > tbody > tr:nth-child(1) > td.dx-treelist-cell-expandable > div.dx-treelist-text-content</value>
   </webElementProperties>
</WebElementEntity>
