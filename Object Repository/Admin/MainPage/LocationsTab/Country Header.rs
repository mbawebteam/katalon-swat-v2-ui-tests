<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Country Header</name>
   <tag></tag>
   <elementGuidId>e7617a07-c834-427d-beab-5930072b3ab4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#dx-col-4 > div.dx-treelist-text-content.dx-text-content-alignment-left</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#dx-col-4 > div.dx-treelist-text-content.dx-text-content-alignment-left</value>
   </webElementProperties>
</WebElementEntity>
