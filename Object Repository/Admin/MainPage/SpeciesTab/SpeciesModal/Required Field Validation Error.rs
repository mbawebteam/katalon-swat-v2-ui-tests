<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Required Field Validation Error</name>
   <tag></tag>
   <elementGuidId>aede2513-0041-4a47-bfc9-3fdb2613d153</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>#formValidationFeedback > ul > li:nth-child(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>#formValidationFeedback > ul > li:nth-child(2)</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Species: Please fill out this field.</value>
   </webElementProperties>
</WebElementEntity>
