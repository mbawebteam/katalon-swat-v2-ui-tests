<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Genus Search Icon</name>
   <tag></tag>
   <elementGuidId>be058183-4cc7-4a41-9fee-fe496b890371</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;gridSpecies&quot;]/div/div[5]/div/table/tbody/tr[2]/td[1]/div/div[1]/div/ul/li/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;gridSpecies&quot;]/div/div[5]/div/table/tbody/tr[2]/td[1]/div/div[1]/div/ul/li/div/div/span</value>
   </webElementProperties>
</WebElementEntity>
