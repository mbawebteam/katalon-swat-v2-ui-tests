<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Genus Column Dropdown Option Equals</name>
   <tag></tag>
   <elementGuidId>678640da-e936-475a-b5c9-b0a9f64e68cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;dx-578d59c3-89c4-7eb8-52dd-e45de2595b7a&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;dx-578d59c3-89c4-7eb8-52dd-e45de2595b7a&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Equals</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>body > div.dx-overlay-wrapper>div.dx-overlay-content dx-resizable dx-context-menu dx-datagrid dx-cell-focus-disabled dx-filter-menu dx-menu-phone-overlay dx-menu-base</value>
   </webElementProperties>
</WebElementEntity>
