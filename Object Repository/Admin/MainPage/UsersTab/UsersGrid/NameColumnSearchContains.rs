<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NameColumnSearchContains</name>
   <tag></tag>
   <elementGuidId>2faad331-7bd8-41c1-a12d-a860ca301eb4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;gridUsers&quot;)/div[@class=&quot;dx-datagrid dx-gridbase-container dx-datagrid-borders&quot;]/div[@class=&quot;dx-datagrid-headers dx-datagrid-nowrap&quot;]/div[@class=&quot;dx-datagrid-content dx-datagrid-scroll-container&quot;]/table[@class=&quot;dx-datagrid-table dx-datagrid-table-fixed&quot;]/tbody[1]/tr[@class=&quot;dx-row dx-column-lines dx-datagrid-filter-row&quot;]/td[@class=&quot;dx-editor-cell&quot;]/div[@class=&quot;dx-editor-with-menu&quot;]/div[@class=&quot;dx-menu dx-widget dx-visibility-change-handler dx-collection dx-menu-base dx-datagrid dx-cell-focus-disabled dx-filter-menu&quot;]/div[@class=&quot;dx-menu-horizontal&quot;]/ul[@class=&quot;dx-menu-items-container&quot;]/li[@class=&quot;dx-menu-item-wrapper&quot;]/div[@class=&quot;dx-item dx-menu-item dx-menu-item-has-icon dx-menu-item-has-submenu dx-state-hover dx-menu-item-expanded&quot;]/div[@class=&quot;dx-context-menu-container-border&quot;]/span[@class=&quot;dx-menu-item-text&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='gridUsers']/div/div[5]/div/table/tbody/tr[2]/td/div/div/div/ul/li/div/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-context-menu-container-border</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;gridUsers&quot;)/div[@class=&quot;dx-datagrid dx-gridbase-container dx-datagrid-borders&quot;]/div[@class=&quot;dx-datagrid-headers dx-datagrid-nowrap&quot;]/div[@class=&quot;dx-datagrid-content dx-datagrid-scroll-container&quot;]/table[@class=&quot;dx-datagrid-table dx-datagrid-table-fixed&quot;]/tbody[1]/tr[@class=&quot;dx-row dx-column-lines dx-datagrid-filter-row&quot;]/td[@class=&quot;dx-editor-cell&quot;]/div[@class=&quot;dx-editor-with-menu&quot;]/div[@class=&quot;dx-menu dx-widget dx-visibility-change-handler dx-collection dx-menu-base dx-datagrid dx-cell-focus-disabled dx-filter-menu&quot;]/div[@class=&quot;dx-menu-horizontal&quot;]/ul[@class=&quot;dx-menu-items-container&quot;]/li[@class=&quot;dx-menu-item-wrapper&quot;]/div[@class=&quot;dx-item dx-menu-item dx-menu-item-has-icon dx-menu-item-has-submenu dx-state-hover dx-menu-item-expanded&quot;]/div[@class=&quot;dx-context-menu-container-border&quot;]/span[@class=&quot;dx-menu-item-text&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='gridUsers']/div/div[5]/div/table/tbody/tr[2]/td/div/div/div/ul/li/div/div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='NGO'])[1]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ROLES'])[1]/following::div[13]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contains'])[1]/preceding::div[8]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Does not contain'])[1]/preceding::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//li/div/div[3]</value>
   </webElementXpaths>
</WebElementEntity>
