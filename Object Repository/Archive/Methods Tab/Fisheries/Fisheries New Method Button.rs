<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Fisheries New Method Button</name>
   <tag></tag>
   <elementGuidId>86768c28-d3aa-41a9-8d83-0872ad1860b3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[(text() = 'New Method' or . = 'New Method')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Method</value>
   </webElementProperties>
</WebElementEntity>
