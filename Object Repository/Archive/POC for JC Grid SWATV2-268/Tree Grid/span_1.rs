<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_1</name>
   <tag></tag>
   <elementGuidId>4efe0f46-0202-42f2-8f23-f34fa9875ab3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;dxFisheries&quot;)/div[@class=&quot;dx-treelist-container dx-gridbase-container dx-treelist-borders&quot;]/div[@class=&quot;dx-treelist-rowsview dx-treelist-nowrap dx-last-row-border&quot;]/div[@class=&quot;dx-treelist-content&quot;]/table[@class=&quot;dx-treelist-table dx-treelist-table-fixed&quot;]/tbody[1]/tr[@class=&quot;dx-row dx-data-row dx-row-lines dx-column-lines&quot;]/td[@class=&quot;dx-treelist-cell-expandable dx-cell-focus-disabled&quot;]/div[@class=&quot;dx-treelist-icon-container&quot;]/div[@class=&quot;dx-treelist-empty-space dx-treelist-collapsed&quot;]/span[1]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dxFisheries']/div/div[6]/div/table/tbody/tr[2]/td/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dxFisheries&quot;)/div[@class=&quot;dx-treelist-container dx-gridbase-container dx-treelist-borders&quot;]/div[@class=&quot;dx-treelist-rowsview dx-treelist-nowrap dx-last-row-border&quot;]/div[@class=&quot;dx-treelist-content&quot;]/table[@class=&quot;dx-treelist-table dx-treelist-table-fixed&quot;]/tbody[1]/tr[@class=&quot;dx-row dx-data-row dx-row-lines dx-column-lines&quot;]/td[@class=&quot;dx-treelist-cell-expandable dx-cell-focus-disabled&quot;]/div[@class=&quot;dx-treelist-icon-container&quot;]/div[@class=&quot;dx-treelist-empty-space dx-treelist-collapsed&quot;]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='dxFisheries']/div/div[6]/div/table/tbody/tr[2]/td/div/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ka'])[2]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//td/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
