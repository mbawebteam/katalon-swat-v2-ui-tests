<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Aquaculture_dx-icon dx-ic</name>
   <tag></tag>
   <elementGuidId>e02a87c9-bd83-4980-b19c-452fecaff2b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='dxFisheries']/div/div[4]/div/div/div[3]/div/div/div/div/div[3]/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dx-icon dx-icon-clear</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dxFisheries&quot;)/div[@class=&quot;dx-treelist-container dx-gridbase-container dx-treelist-borders&quot;]/div[@class=&quot;dx-treelist-header-panel&quot;]/div[@class=&quot;dx-toolbar dx-widget dx-visibility-change-handler dx-collection&quot;]/div[@class=&quot;dx-toolbar-items-container&quot;]/div[@class=&quot;dx-toolbar-after&quot;]/div[@class=&quot;dx-item dx-toolbar-item dx-toolbar-button&quot;]/div[@class=&quot;dx-item-content dx-toolbar-item-content&quot;]/div[@class=&quot;dx-treelist-search-panel dx-textbox dx-texteditor dx-editor-outlined dx-searchbox dx-show-clear-button dx-widget dx-state-hover&quot;]/div[@class=&quot;dx-texteditor-container&quot;]/div[@class=&quot;dx-texteditor-buttons-container&quot;]/span[@class=&quot;dx-clear-button-area&quot;]/span[@class=&quot;dx-icon dx-icon-clear&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='dxFisheries']/div/div[4]/div/div/div[3]/div/div/div/div/div[3]/span/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aquaculture'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fisheries'])[2]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Parent Gear Type'])[1]/preceding::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fao Method'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//span/span</value>
   </webElementXpaths>
</WebElementEntity>
