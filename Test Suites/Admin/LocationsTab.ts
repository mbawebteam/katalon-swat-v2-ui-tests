<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LocationsTab</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f8c788c9-be01-46a9-b4eb-e4fe9c588ec3</testSuiteGuid>
   <testCaseLink>
      <guid>9f33c429-20bc-4b80-8cec-e262d3306c01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin/MainPage/LocationsTab/Country Sub-division Tab/Verify Country sub-division column headers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b596e96d-97ed-46eb-97e9-ce035c91773c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin/MainPage/LocationsTab/Country Sub-division Tab/Worldwide value is first entry in Country column</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
