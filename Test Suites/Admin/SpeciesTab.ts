<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SpeciesTab</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8019bc53-e2e3-48be-90f5-ab5fb4cda6fb</testSuiteGuid>
   <testCaseLink>
      <guid>d9a01f3a-6e97-43d2-8dbc-74a2a4dbe3c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin/MainPage/SpeciesTab/SpeciesModal/Create Species modal shows all required fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41365251-887f-4724-bf35-1493cf80e213</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin/MainPage/SpeciesTab/SpeciesModal/Create Species with Japanese Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae384492-55b5-4492-8f84-bdcf83ee3d1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin/MainPage/SpeciesTab/SpeciesModal/Create Species with Latin Characters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9f295a7-5e04-4660-a93d-f1de9662a621</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin/MainPage/SpeciesTab/SpeciesModal/Create Species with only required field values</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e266fa1e-bf18-451d-b03c-21df9e59bca1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin/MainPage/SpeciesTab/SpeciesModal/Create Species without all required field values</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
